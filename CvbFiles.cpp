/*  (c) Copyright:  2009  CVB, Confidential Data
**
**  $Workfile:          CvbFiles.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Misc helper functions
**
**
**  Entry Points:       CVB_GetTimeDateStamp()
**                      CVB_GetTimeStamp()
**                      CVB_GetDateStamp()
**
**
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       16Feb2003
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "CvbFiles.h"
#include "ClockApi.h"


const char cLutUnicodeC3[128] =
{  //  x0   x1   x2   x3   x4   x5   x6   x7   x8   x9   xA   xB   xC   xD   xE    xF
      'A', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I',  'I',  // 8x
      'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', '?',  'S',  // 9x
      'a', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i',  'i',  // Ax
      '?', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y',  'y',  // Bx
      '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?',  '?',  // Cx
      '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?',  '?',  // Dx
      '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?',  '?',  // Ex
      '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?',  '?'   // Fx
};

const char cLutUnicodeC4[128] =
{  //  x0   x1   x2   x3   x4   x5   x6   x7   x8   x9   xA   xB   xC   xD   xE    xF
      'A', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I',  'I',  // 8x
      'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', '?',  'S',  // 9x
      'a', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i',  'i',  // Ax
      '?', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y',  'y',  // Bx
      '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?',  '?',  // Cx
      '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?',  '?',  // Dx
      '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?',  '?',  // Ex
      '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?',  '?'   // Fx
};

//
//  Function:   CVB_GetTimeDateFilename
//  Purpose:    Rework system T&D to a usable unique filename
//  Parms:      void
//  Returns:    T&D: "20110720-115959"
//
CString CVB_GetTimeDateFilename(void)
{
    CString s;

    CTime t = CTime::GetCurrentTime();

    s.Empty();
    s += t.Format("%Y%d%m");
    s += "-";
    s += t.Format("%H%M%S");

    return(s);
}

//
//  Function:   CVB_GetTimeDateStamp
//  Purpose:    Rework system T&D to ascii
//  Parms:      void
//  Returns:    T&D: "24/12/2007 09:12:25"
//
CString CVB_GetTimeDateStamp(void)
{
    CString s;

    CTime t = CTime::GetCurrentTime();

    s.Empty();
    s += t.Format("%d/%m/%Y");
    s += " ";
    s += t.Format("%H:%M:%S");

    return(s);
}

//
//  Function:   GetDateStamp
//  Purpose:    Rework system T&D to ascii
//  Parms:      void
//  Returns:    T&D: "24/12/2007"
//
CString CVB_GetDateStamp(void)
{
    CString s;

    CTime t = CTime::GetCurrentTime();

    s.Empty();
    s += t.Format("%d/%m/%Y");

    return(s);
}

//
//  Function:   CVB_GetTimeStamp
//  Purpose:    Rework system T&D to ascii
//  Parms:      void
//  Returns:    T&D: "09:12:25"
//
CString CVB_GetTimeStamp(void)
{
    CString s;

    CTime t = CTime::GetCurrentTime();

    s.Empty();
    s += t.Format("%H:%M:%S");
    return(s);
}


//
//  Function:   CVB_SafeMalloc
//  Purpose:    Allocate a block of memory, clear all data
//  Parms:      Size
//  Returns:    Pointer to the block
//     Note:    The safe function keeps the size at the start of the memory block
//
static size_t tAllocated = 0;

void *CVB_SafeMalloc(size_t tSize)
{
   void    *pvData = NULL;

   if(tSize)
   {
      tSize += 8;
      pvData = malloc(tSize);
      tAllocated += tSize;
      if(pvData)
      {
         memset(pvData, 0, tSize);
         pvData = CVB_ulong_put((u_int8 *)pvData, (u_int32)0xdeadbeef);
         pvData = CVB_ulong_put((u_int8 *)pvData, (u_int32)tSize);
      }
   }
   return(pvData);
}

//
//  Function:   CVB_SafeFree
//  Purpose:    Free the block of memory
//  Parms:      Pointer to the block
//  Returns:    NULL
//
void *CVB_SafeFree(void *pvData)
{
   u_int32  ulSig, ulTemp;
   u_int8  *pubTemp;

   pubTemp = (u_int8 *)pvData;
   //
   if(pubTemp)
   {
      pubTemp -= 8;
      ulSig  = CVB_ulong_get(&pubTemp[0], 0xffffffff, 0);
      if(ulSig != 0xdeadbeef)
      {
         AfxMessageBox("Malloc-Free signature error !");
         return(NULL);
      }
      ulTemp = CVB_ulong_get(&pubTemp[4], 0xffffffff, 0);
      if(ulTemp > tAllocated)
      {
         AfxMessageBox("Malloc-Free issues !");
         return(NULL);
      }
      tAllocated -= (size_t) ulTemp;
      free(pubTemp);
   }
   return(NULL);
}

//
//  Function:   CVB_GetAllocatedMemory
//  Purpose:    Get the current total amount of allocated memory
//  Parms:
//  Returns:    size of allocated memory
//
u_int32 CVB_GetAllocatedMemory(void)
{
   return((u_int32) tAllocated);
}

//
//  Function:   CVB_SafeGetMallocSize
//  Purpose:    Get the size of a block of allocated memory
//  Parms:      Pointer to the block
//  Returns:    Size
//
u_int32 CVB_SafeGetMallocSize(void *pvData)
{
   u_int32  ulSig, ulSize=0;
   u_int8  *pubTemp;

   pubTemp = (u_int8 *)pvData;
   //
   if(pubTemp)
   {
      pubTemp -= 8;
      ulSig  = CVB_ulong_get(&pubTemp[0], 0xffffffff, 0);
      if(ulSig != 0xdeadbeef)
      {
         AfxMessageBox("Malloc-Free signature error !");
      }
      else
      {
         ulSize = CVB_ulong_get(&pubTemp[4], 0xffffffff, 0);
      }
   }
   return(ulSize);
}

/**
 **  Name:         CVB_ascii_to_ulong
 **
 **  Description:  Converts an ascii-string into an ulong value.
 **
 **  Arguments:    string ^, value ^
 **
 **  Returns:      Address after converted value.
 **
 **/
char *CVB_ascii_to_ulong(char *pcValue, u_int32 *pulValue)
{
   u_int8 ubIndex = 0;

   //
   // Reset value
   //
   *pulValue = 0;

   while  (
            (isdigit(*pcValue))
            &&
            (ubIndex++ < 10)
          )
   {
      *pulValue *= 10;
      *pulValue += (u_int32) ((u_int32) *pcValue++ - (u_int32) '0');
   }

   return (pcValue);
}

/**
 **  Name:         CVB_ascii_to_ushort
 **
 **  Description:  Converts an ascii-string into an ushort value.
 **
 **  Arguments:    string ^, value ^
 **
 **  Returns:      Address after converted value.
 **
 **/

char *CVB_ascii_to_ushort(char *pcValue, u_int16 *usValue)
{
   u_int8 ubIndex = 0;

   //
   // Reset value
   //
   *usValue = 0;

   while  (
            (isdigit(*pcValue))
            &&
            (ubIndex++ < 5)
          )
   {
      *usValue *= 10;
      *usValue += (u_int16) (*pcValue++ - '0');
   }

   return (pcValue);
}

/**
 **  Name:         CVB_BCD_to_ulong
 **
 **  Description:  Convert BCD to u_int32
 **
 **  Arguments:    32 bits BCD
 **
 **  Returns:      u_int32
 **/
u_int32 CVB_BCD_to_ulong(u_int32 ulBcd)
{
   u_int32    ulHex, ulFactor;
   u_int8    nr;

   ulFactor = 1L;
   ulHex    = 0;

   for(nr = 0; nr < 8; nr++)
   {
      ulHex += (ulBcd & 0xf) * ulFactor;
      ulFactor *= 10;
      ulBcd >>= 4;
   }
   return (ulHex);
}

/**
 **  Name:         CVB_BCD_to_ushort
 **
 **  Description:  Convert BCD to u_int16
 **
 **  Arguments:    16 bits BCD
 **
 **  Returns:      u_int32
 **/
u_int16 CVB_BCD_to_ushort(u_int16 usBcd)
{
   u_int16   usHex, usFactor;
   u_int8    nr;

   usFactor = 1;
   usHex    = 0;

   for(nr = 0; nr < 4; nr++)
   {
      usHex += (usBcd & 0xf) * usFactor;
      usFactor *= 10;
      usBcd >>= 4;
   }
   return (usHex);
}

/**
 **  Name:         CVB_ubyte_get
 **
 **  Description:  Gets a ubyte from a byte array
 **
 **  Arguments:    array ^, mask, shift
 **
 **  Returns:      u_int8
 **/
u_int8 CVB_ubyte_get(u_int8 *pubSrc, u_int8 ubMask, u_int8 ubShift)
{
   u_int8 ubTemp;

   ubTemp   = *pubSrc;
   ubTemp >>= ubShift;
   ubTemp  &= ubMask;

   return (ubTemp);
}

/**
 **  Name:         CVB_ubytes_put
 **
 **  Description:  Puts ubytes in a byte array
 **
 **  Arguments:    dest^, src^, nr
 **
 **  Returns:      new dest^
 **/
u_int8 *CVB_ubytes_put(u_int8 *pubDest, u_int8 *pubSrc, int iSize)
{
   return ( CVB_ubytes_get(pubDest, pubSrc, iSize) );
}

/**
 **  Name:         CVB_ubytes_get
 **
 **  Description:  Gets ubytes from a byte array
 **
 **  Arguments:    dest^, src^, nr
 **
 **  Returns:      new dest^
 **/
u_int8 *CVB_ubytes_get(u_int8 *pubDest, u_int8 *pubSrc, int iSize)
{
   int   i;

   for(i=0; i<iSize; i++)
   {
      *pubDest++ = *pubSrc++;
   }
   return (pubDest);
}

/**
 **  Name:         CVB_ulong_get
 **
 **  Description:  Gets a ulong from a byte array
 **
 **  Arguments:    array ^, mask, shift
 **
 **  Returns:      u_int32
 **/
u_int32 CVB_ulong_get(u_int8 *pubSrc, u_int32 ulMask, u_int8 ubShift)
{
   u_int32 ulTemp;

   ulTemp = ((u_int32)*pubSrc       << 24) +
            ((u_int32)*(pubSrc + 1) << 16) +
            ((u_int32)*(pubSrc + 2) <<  8) +
             (u_int32)*(pubSrc + 3);
   ulTemp >>= ubShift;
   ulTemp &= ulMask;

   return (ulTemp);
}

/**
 **  Name:         CVB_ubyte_put
 **
 **  Description:  Puts a ubyte into a byte array
 **
 **  Arguments:    array ^, value
 **
 **  Returns:      Updated array ptr.
 **/

u_int8 *CVB_ubyte_put(u_int8 *ubPtr, u_int8 ubValue)
{
   *ubPtr++ = ubValue;
   return (ubPtr);
}


/**
 **  Name:         CVB_ulong_put
 **
 **  Description:  Puts a ulong into a byte array
 **
 **  Arguments:    array ^, value
 **
 **  Returns:      Updated array ptr.
 **/

u_int8 *CVB_ulong_put(u_int8 *ubPtr, u_int32 ulValue)
{
   ubPtr[0] = (u_int8)(ulValue >> 24);
   ubPtr[1] = (u_int8)(ulValue >> 16);
   ubPtr[2] = (u_int8)(ulValue >> 8);
   ubPtr[3] = (u_int8)(ulValue);
   return (ubPtr+4);
}

/**
 **  Name:         CVB_ulong_to_BCD
 **
 **  Description:  Converts a u_int32 value into it's BCD representation.
 **
 **  Arguments:    Value to convert
 **
 **  Returns:      Converted value
 **/

u_int32 CVB_ulong_to_BCD(u_int32 ulValue)
{
   u_int8 ubIndex,
         ubShift = 0;

   u_int32 ulBcdDigit,
         ulRet = 0;

   for (ubIndex = 0; ubIndex < 8; ubIndex++)
   {
      ulBcdDigit   = ulValue % 10;
      ulBcdDigit <<= ubShift;
      ulRet       |= ulBcdDigit;
      ulValue     /= 10;
      ubShift     += 4;
   }

   return (ulRet);
}

/**
 **  Name:         CVB_ushort_get
 **
 **  Description:  Get's ushort value out of byte array
 **
 **  Arguments:    array ^, mask, shift
 **
 **  Returns:      value
 **/

u_int16 CVB_ushort_get(u_int8 *ubPtr, u_int16 usMask, u_int8 ubShift)
{
   u_int16 usTemp;

   usTemp = (*ubPtr << 8) + *(ubPtr + 1);
   usTemp >>= ubShift;
   usTemp  &= usMask;

   return (usTemp);
}

/**
 **  Name:         CVB_ushort_put
 **
 **  Description:  Puts a ushort into a byte array
 **
 **  Arguments:    array ^, value
 **
 **  Returns:      Updated array ptr.
 **/

u_int8 *CVB_ushort_put(u_int8 *ubPtr, u_int16 usValue)
{
   ubPtr[0] = (u_int8) (usValue >> 8);
   ubPtr[1] = (u_int8) (usValue);
   return (ubPtr+2);
}


/**
 **  Name:         CVB_ushort_to_BCD
 **
 **  Description:  Converts a u_int16 value into it's BCD representation.
 **
 **  Arguments:    Value to convert
 **
 **  Returns:      Converted value
 **/

u_int16 CVB_ushort_to_BCD(u_int16 usValue)
{
   u_int8  ubIndex,
          ubShift = 0;

   u_int16 usBcdDigit,
          usRet = 0;

   for (ubIndex = 0; ubIndex < 4; ubIndex++)
   {
      usBcdDigit   = usValue % 10;
      usBcdDigit <<= ubShift;
      usRet       |= usBcdDigit;
      usValue     /= 10;
      ubShift     += 4;
   }

   return (usRet);
}

/**
 **  Name:         CVB_ushort_update
 **
 **  Description:  Updates a ushort in a byte array with an offset
 **
 **  Arguments:    array ^, offset
 **
 **  Returns:      None
 **/

void CVB_ushort_update(u_int8 *ubPtr, u_int16 usOffset)
{
   u_int16 usValue;

   //
   // Get value from array
   //
   usValue  = CVB_ushort_get(ubPtr, 0xFFFF, 0);

   //
   // Update the value
   //
   usValue += usOffset;

   //
   // Put updated value back into array
   //
   CVB_ushort_put(ubPtr, usValue);
}

/**
 **  Name:         CVB_llong_get
 **
 **  Description:  Gets a int64 (long long) from a byte array
 **
 **  Arguments:    array ^, mask, shift
 **
 **  Returns:      llong
 **/
int64 CVB_llong_get(u_int8 *pubSrc, int64 llMask, u_int8 ubShift)
{
   int64 llTemp;

   llTemp = ((int64)*pubSrc       << 56) +
            ((int64)*(pubSrc + 1) << 48) +
            ((int64)*(pubSrc + 2) << 40) +
            ((int64)*(pubSrc + 3) << 32) +
            ((int64)*(pubSrc + 4) << 24) +
            ((int64)*(pubSrc + 5) << 16) +
            ((int64)*(pubSrc + 6) <<  8) +
             (int64)*(pubSrc + 7);
   llTemp >>= ubShift;
   llTemp &= llMask;

   return (llTemp);
}

/**
 **  Name:         CVB_llong_put
 **
 **  Description:  Puts a int64 (long long) into a byte array
 **
 **  Arguments:    array ^, value
 **
 **  Returns:      Updated array ptr.
 **/
u_int8 *CVB_llong_put(u_int8 *ubPtr, int64 llValue)
{
   *ubPtr++ = (u_int8)(llValue >> 56);
   *ubPtr++ = (u_int8)(llValue >> 48);
   *ubPtr++ = (u_int8)(llValue >> 40);
   *ubPtr++ = (u_int8)(llValue >> 32);
   *ubPtr++ = (u_int8)(llValue >> 24);
   *ubPtr++ = (u_int8)(llValue >> 16);
   *ubPtr++ = (u_int8)(llValue >> 8);
   *ubPtr++ = (u_int8)(llValue);
   //
   return (ubPtr);
}

/**
 **  Name:         CVB_CountWords
 **
 **  Description:  Count the number of words in a string
 **
 **  Arguments:    buffer ptr, min word size
 **
 **  Returns:      Count
 **/
int CVB_CountWords(char *pcRecord, int iMinLen)
{
   int   iCount=0, iLen=0;
   BOOL  fNewWord=TRUE;

   if(pcRecord && *pcRecord)
   {
      while(*pcRecord)
      {
         if( isalnum(*pcRecord) )
         {
            if(++iLen >= iMinLen)
            {
               if(fNewWord)
               {
                  iCount++;
               }
               fNewWord = FALSE;
            }
         }
         else
         {
            fNewWord = TRUE;
            iLen     = 0;
         }
         pcRecord++;
      }
   }
   return(iCount);
}

/**
 **  Name:         CVB_CountWords
 **
 **  Description:  Count the number of words in a string
 **
 **  Arguments:    buffer ptr, min word size, number of letters in the words
 **
 **  Returns:      Count
 **/
int CVB_CountWords(char *pcRecord, int iMinLen, int *piLetters)
{
   int   iCount=0, iLen=0, iLetters=0;
   BOOL  fNewWord=TRUE;

   if(pcRecord)
   {
      while(*pcRecord)
      {
         if( isalnum(*pcRecord) )
         {
            if(++iLen >= iMinLen)
            {
               if(fNewWord)
               {
                  iCount++;
                  iLetters += iLen;
               }
               else iLetters++;
               fNewWord = FALSE;
            }
         }
         else
         {
            fNewWord = TRUE;
            iLen     = 0;
         }
         pcRecord++;
      }
   }
   if(piLetters) *piLetters = iLetters;
   return(iCount);
}

/**
 **  Name:        CVB_GetWord
 **
 **  Description: Get one word from a string
 **
 **  Arguments:   String ptr, word nr 1..?, min word size
 **
 **  Returns:     Word ptr
 **/
char *CVB_GetWord(char *pcRecord, int iIndex, int iMinLen)
{
   int   iCount=1, iLen=0;
   BOOL  fNewWord=TRUE;
   BOOL  fNxtWord=TRUE;
   char *pcResult=NULL;
   char *pcTemp=NULL;

   if(pcRecord && *pcRecord)
   {
      while(*pcRecord)
      {
         if( isalnum(*pcRecord) )
         {
            if(fNxtWord) pcTemp = pcRecord;
            fNxtWord = FALSE;
            if(++iLen >= iMinLen)
            {
               if(fNewWord && (iCount == iIndex) )
               {
                  pcResult = pcTemp;
                  break;
               }
               fNewWord = FALSE;
            }
            pcRecord++;
         }
         else
         {
            if(!fNewWord) iCount++;
            fNewWord = TRUE;
            fNxtWord = TRUE;
            iLen     = 0;
            pcRecord++;
         }
      }
   }
   return(pcResult);
}

/**
 **  Name:        CVB_GetWordLength
 **
 **  Description: Get the length of a word from a string
 **
 **  Arguments:   String ptr
 **
 **  Returns:     Length
 **/
int CVB_GetWordLength(char *pcRecord)
{
   int   iLen=0;

   if(pcRecord && *pcRecord)
   {
      while(*pcRecord)
      {
         if( isalnum(*pcRecord++) ) iLen++;
         else                       break;
      }
   }
   return(iLen);
}

/**
 **  Name:         CVB_DeleteChar
 **
 **  Description:  Delete char from the buffer
 **
 **  Arguments:    buffer ptr
 **
 **  Returns:
 **/
void CVB_DeleteChar(char *pcBuffer)
{
   if(pcBuffer)
   {
      *pcBuffer = '\0'; // in case last char !!
      strcpy(pcBuffer, pcBuffer+1);
   }
}

/**
 **  Name:         CVB_RemoveChar
 **
 **  Description:  Remove a particular char from the buffer
 **
 **  Arguments:    buffer ptr, char
 **
 **  Returns:      number removed
 **/
int CVB_RemoveChar(char *pcBuffer, char cRemove)
{
   int   iNr = 0;

   while(pcBuffer && *pcBuffer)
   {
      if(*pcBuffer == cRemove)
      {
         CVB_DeleteChar(pcBuffer);
         iNr++;
      }
      else
      {
         pcBuffer++;
      }
   }
   return(iNr);
}

/**
 **  Name:         CVB_RemoveChars
 **
 **  Description:  Remove particular chars
 **
 **  Arguments:    buffer ptr, char list
 **
 **  Returns:      number replaced
 **/
int CVB_RemoveChars(char *pcBuffer, char *pcRemove)
{
   int   iNr = 0;

   while(pcRemove && *pcRemove)
   {
      iNr += CVB_RemoveChar(pcBuffer, *pcRemove);
      pcRemove++;
   }
   return(iNr);
}

/**
 **  Name:         CVB_ReplaceChar
 **
 **  Description:  Replace a particular char
 **
 **  Arguments:    buffer ptr, char, new char
 **
 **  Returns:      number replaced
 **/
int CVB_ReplaceChar(char *pcBuffer, char cRemove, char cNew)
{
   int   iNr = 0;

   while(pcBuffer && *pcBuffer)
   {
      if(*pcBuffer == cRemove)
      {
         *pcBuffer++ = cNew;
         iNr++;
      }
      else
      {
          pcBuffer++;
      }
   }
   return(iNr);
}

/**
 **  Name:         CVB_ReplaceChars
 **
 **  Description:  Replace chars
 **
 **  Arguments:    buffer ptr, charlist, new char
 **
 **  Returns:      number replaced
 **/
int CVB_ReplaceChars(char *pcBuffer, char *pcReplace, char cNew)
{
   int   iNr = 0;

   while(pcReplace && *pcReplace)
   {
      iNr += CVB_ReplaceChar(pcBuffer, *pcReplace, cNew);
      pcReplace++;
   }
   return(iNr);
}

/**
 **  Name:         CVB_ReplaceUnicodeChars
 **
 **  Description:  Replace all UTF8 unicode chars
 **
 **  Arguments:    buffer ptr
 **
 **  Returns:      number replaced
 **/
int CVB_ReplaceUnicodeChars(char *pcBuffer)
{
   int         iNr=0;
   char        cNew;

   while(*pcBuffer)
   {
      // ??? if(!isprint(*pcBuffer))  // Assert error on >0x7F
      if(*pcBuffer & 0x80)                //
      {
         if(*pcBuffer == (char)0xC3)
         {
            CVB_DeleteChar(pcBuffer);
            cNew        = *pcBuffer;
            *pcBuffer++ = cLutUnicodeC3[cNew & 0x7F];
            iNr++;
         }
         else if(*pcBuffer == (char)0xC4)
         {
            CVB_DeleteChar(pcBuffer);
            cNew        = *pcBuffer;
            *pcBuffer++ = cLutUnicodeC4[cNew & 0x7F];
            iNr++;
         }
         else if(*pcBuffer == (char)0xE2)
         {
            CVB_DeleteChar(pcBuffer);
            CVB_DeleteChar(pcBuffer);
            *pcBuffer++ = '-';
            iNr++;
         }
         else
         {
            // No idea what this is: get rid of it.
            CVB_DeleteChar(pcBuffer);
            iNr++;
         }
      }
      else pcBuffer++;
   }
   return(iNr);
}

//
//  Function:  CVB_RemoveLinebreaks
//  Purpose:   Remove newlines and replace by char
//
//  Parms:     buffer, buffer size
//
//  Returns:   Nr of linebreaks
//
int CVB_RemoveLinebreaks(char *pcData, int iSize, char cReplace)
{
   int  iNr=0;

   while(iSize)
   {
      if(*pcData == 0x0d)
      {
         // Remove the CR
         CVB_RemoveChar(pcData, 0x0d);
         iSize--;
         iNr++;
      }
      if(*pcData == 0x0a)
      {
         // Replace the LF by CHAR
         CVB_ReplaceChar(pcData, 0x0a, cReplace);
      }
      pcData++;
      iSize--;
   }
   return(iNr);
}

//
//  Function:  CVB_ExecuteCommand
//  Purpose:   Spawn an external program
//
//  Parms:     Command + parameters
//
//  Returns:   Process HANDLE of new process
//
HANDLE CVB_ExecuteCommand(CString *pclExec)
{
  SECURITY_ATTRIBUTES   secattr;
  STARTUPINFO           sInfo;
  HANDLE                rPipe, wPipe;
  PROCESS_INFORMATION   pInfo;
  char                  pcCommand[1024];

  ZeroMemory(&secattr, sizeof(secattr));
  secattr.nLength        = sizeof(secattr);
  secattr.bInheritHandle = TRUE;
  //
  //Create pipes to write and read data
  //
  CreatePipe(&rPipe, &wPipe, &secattr, 0);
  //
  ZeroMemory(&sInfo,sizeof(sInfo));
  ZeroMemory(&pInfo,sizeof(pInfo));
  sInfo.cb           = sizeof(sInfo);
  sInfo.dwFlags      = STARTF_USESTDHANDLES;
  sInfo.hStdInput    = NULL;
  sInfo.hStdOutput   = wPipe;
  sInfo.hStdError    = wPipe;

  strcpy(pcCommand, pclExec->GetBuffer(pclExec->GetLength()));

  //Create the process here.
  CreateProcess(0, pcCommand,0,0,TRUE, NORMAL_PRIORITY_CLASS|CREATE_NO_WINDOW,0,0,&sInfo,&pInfo);
  CloseHandle(wPipe);
  return(pInfo.hProcess);
}

//
//  Function:  CVB_ExecuteShell
//  Purpose:   Execute shell command and await completion
//
//  Parms:     Command + parameters
//
//  Returns:   int CC
//
int CVB_ExecuteShell(CString *pclExec, CString *pclParm)
{
   return( CVB_ExecuteShell(pclExec->GetBuffer(), pclParm->GetBuffer()) );
}

//
//  Function:  CVB_ExecuteShell
//  Purpose:   Execute shell command and await completion
//
//  Parms:     Command + parameters
//
//  Returns:   int CC
//
int CVB_ExecuteShell(char *pcExec, char *pcParm)
{
   int              iCc;
   SHELLEXECUTEINFO pstExecInfo = {0};

   pstExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
   pstExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
   pstExecInfo.hwnd = NULL;
   pstExecInfo.lpVerb = NULL;
   pstExecInfo.lpFile = pcExec;
   pstExecInfo.lpParameters = pcParm;
   pstExecInfo.lpDirectory = NULL;
   pstExecInfo.nShow = SW_SHOW;
   pstExecInfo.hInstApp = NULL;
   iCc = ShellExecuteEx(&pstExecInfo);
   //
   // Wait until the command has finished
   //
   WaitForSingleObject(pstExecInfo.hProcess,INFINITE);
   return(iCc);
}

//
//  Function:  CVB_CheckProcessCompletion
//  Purpose:   Check if a child process has finished
//
//  Parms:     Process handle, wait YESNO
//  Returns:    0: process busy
//             +1: process terminated OKee
//             -1: process error
//
int CVB_CheckProcessCompletion(HANDLE tHandle, BOOL fWait)
{
   int   iCc=0;
   DWORD dwExitCode=0;

   while(TRUE)
   {
      if( GetExitCodeProcess(tHandle, &dwExitCode) )
      {
         if(dwExitCode == STILL_ACTIVE)
         {
            if(fWait) Sleep(100);
            else      break;
         }
         else
         {
            iCc = 1;
            break;
         }
      }
      else
      {
         iCc = -1;
         break;
      }
   }
   return(iCc);
}

//
//  Function:  CVB_GetFilesInDirectories
//  Purpose:   Obtain all (first/next) file from all directories (nested)
//  Parms:     Handle storage, Input Path, Result File-Path, Result Dir-Path 
//  Returns:   Legal handle (!INVALID_HANDLE_VALUE) if first/next file found
//
//
DIRS *CVB_GetFilesInDirectories(DIRS *pstDirs, CString *pclPath, CString *pclStrFile, CString *pclStrDir)
{
   BOOL  fSearching=TRUE;
   int   iLevel, iIdx;
   DIRS *pstNew=pstDirs;

   if(pstDirs) 
   {
      iLevel = pstDirs->iLevel;
   }
   else
   {
      iLevel = 0;
      pclStrDir->Empty();
      pclStrFile->Empty();
   }
   //
   while(fSearching)
   {
      if(pstNew == NULL)
      {
         //
         // Entry point for recursive file find
         //
         pstNew = new(DIRS);
         //
         pstNew->iLevel  = ++iLevel;
         pstNew->tHandle = INVALID_HANDLE_VALUE;
         pstNew->pclPath = new(CString);
         pstNew->pstPrev = pstDirs;
         pstDirs         = pstNew;
         //
         // Remove the wildcard (if any) from the input path and add the new directory to nest the search
         //
         iIdx = pclPath->Find("\\*.*");
         if(iIdx != -1)
         {
            //
            // Remove the wildcards from the pathname
            //
            pclPath->Delete(iIdx,4);
         }
         if(pclStrDir->GetLength() != 0)
         {
            *pclPath += "\\";
            *pclPath += *pclStrDir;
        }
         *pstDirs->pclPath = *pclPath;
         *pclPath += "\\*.*";
      }
      //
      // Get next file/dir entry
      //
      pstDirs->tHandle = CVB_GetFileInDirectory(pstDirs->tHandle, pclPath, pclStrFile, pclStrDir);
      //
      if(pstDirs->tHandle == INVALID_HANDLE_VALUE)
      {
         //
         // This level is done: back to previous
         //
         if(--pstDirs->iLevel)
         {
            pstNew   = pstDirs->pstPrev;
            *pclPath = *pstNew->pclPath;
            delete(pstDirs->pclPath);
            delete(pstDirs);
            pstDirs  = pstNew;
         }
         else
         {
            //
            // No more files, no previous level: Done !
            //
            pstDirs = NULL;
            pclStrFile->Empty();
            pclStrDir->Empty();
            fSearching = FALSE;
         }
      }
      else
      {
         //
         // Check if we have a file or dir entry
         //
         if(pclStrFile->GetLength() == 0)
         {
            //
            // We have a dir entry
            //
            if(pclStrDir->GetAt(0) != '.')
            {
               //
               // This is not a system dir ("." or "..")
               //
               pstNew = NULL;
            }
            else
            {
               // .. or .
            }
         }
         else
         {
            //
            // We have a file entry
            //
            *pclStrDir = *pstDirs->pclPath;
            fSearching = FALSE;
         }
      }
   }
   return(pstDirs);
}

//
//  Function:  CVB_GetFileInDirectory
//  Purpose:   Obtain a (first/next) file from a directory
//  Parms:     Handle, Input Path, Result File-Path, Result Dir-Path (or NULL)
//  Returns:   Legal handle (!INVALID_HANDLE_VALUE) if first/next file found
//
//
HANDLE CVB_GetFileInDirectory(HANDLE tHandle, CString *pclPath, CString *pclStrFile, CString *pclStrDir)
{
   BOOL              fCC=FALSE, fFoundFile=FALSE;
   WIN32_FIND_DATA   stResult;

   while(!fFoundFile)
   {
      if(tHandle == INVALID_HANDLE_VALUE)
      {
         tHandle = FindFirstFile(pclPath->GetBuffer(), &stResult);
         fCC = (tHandle != INVALID_HANDLE_VALUE);
      }
      else
      {
         fCC = FindNextFile(tHandle, &stResult);
      }
      //
      if(fCC)
      {
         //
         // We have File or Dir info
         //
         if(stResult.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
         {
            // This is a directory
            if(pclStrDir)
            {
                pclStrFile->Empty();
               *pclStrDir  = stResult.cFileName;
                fFoundFile = TRUE;
            }
         }
         else
         {
            // This is a file
            if(pclStrDir) pclStrDir->Empty();
           *pclStrFile = stResult.cFileName;
            fFoundFile = TRUE;
         }
      }
      else
      {
         if (tHandle != INVALID_HANDLE_VALUE)
         {
            FindClose(tHandle);
            tHandle = INVALID_HANDLE_VALUE;
         }
         fFoundFile = TRUE;
      }
   }
   return(tHandle);
}

//
//  Function:  CVB_GetFileStatus
//  Purpose:   Obtain some status info from a file:
//                exists YESNO
//                Filesize
//                Creation secs from 1900
//  Parms:     Pathname, Secs^, Size^
//  Returns:   TRUE is file exists
//
//  Note:      Cfile.GetStatus does not support files >> 4 GB !
//
BOOL CVB_GetFileStatus(char *pcPath, u_int32 *pulSecs, u_int64 *pllSize)
{
   BOOL                 fExists;
   HANDLE               tHdl;
   FILETIME             stCrea;
   CLOCK_COUNT_INFO     stCount;
   CLOCK_DATE_TIME_INFO stDateTime;
   SYSTEMTIME           stSysTime;
   u_int32              ulFileSizeHigh=0, ulFileSizeLow;
   u_int64              llSize;

   tHdl = CreateFile(pcPath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
   if(tHdl != INVALID_HANDLE_VALUE)
   {
      fExists = TRUE;
      GetFileTime(tHdl, &stCrea, NULL, NULL);
      ulFileSizeLow = GetFileSize(tHdl, (LPDWORD) &ulFileSizeHigh);
      if(pllSize)
      {
         llSize  = (((u_int64) ulFileSizeHigh) << 32) + ulFileSizeLow;
        *pllSize = llSize;
      }

      CloseHandle(tHdl);

      FileTimeToSystemTime(&stCrea, &stSysTime);

      stDateTime.usYear   = (u_int16) stSysTime.wYear - 1900;
      stDateTime.ubMonth  = (u_int8)  stSysTime.wMonth;
      stDateTime.ubDay    = (u_int8)  stSysTime.wDay;
      stDateTime.ubHour   = (u_int8)  stSysTime.wHour;
      stDateTime.ubMin    = (u_int8)  stSysTime.wMinute;
      stDateTime.ubSec    = (u_int8)  stSysTime.wSecond;

      CLOCK_UtilDateTimeToCount(&stDateTime, &stCount);

      if(pulSecs) *pulSecs = stCount.ulSeconds;
   }
   else
   {
      fExists = FALSE;
      if(pulSecs) *pulSecs = 0;
   }
   return(fExists);
}

/**
 **  Name:         CVB_RightTrim
 **
 **  Description:  Trim trailing spaces
 **
 **  Arguments:    buffer ptr
 **
 **  Returns:      buffer ptr just after last non-space
 **/
char *CVB_RightTrim(char *pcBuffer)
{
   int   x;
   char *pcRes=NULL;

   if(pcBuffer)
   {
      x = (int) strlen(pcBuffer);

      while(x)
      {
         if(pcBuffer[x-1] == ' ')
         {
            x--;
            pcBuffer[x] = 0;
         }
         else break;
      }
      pcRes = &pcBuffer[x];
   }
   return(pcRes);
}

/**
 **  Name:         CVB_LeftTrim
 **
 **  Description:  Trim leading spaces
 **
 **  Arguments:    buffer ptr
 **
 **  Returns:      buffer ptr
 **/
char *CVB_LeftTrim(char *pcBuffer)
{
   while(*pcBuffer == ' ')
   {
      pcBuffer++;
   }
   return(pcBuffer);
}

/**
 **  Name:         CVB_TrimTillSpace
 **
 **  Description:  Trim non spaces
 **
 **  Arguments:    buffer ptr
 **
 **  Returns:      buffer ptr
 **/
char *CVB_TrimTillSpace(char *pcBuffer)
{
   while(*pcBuffer && (*pcBuffer != ' ') )
   {
      pcBuffer++;
   }
   return(pcBuffer);
}

//
//  Function:  CVB_SecsToDateTime
//  Purpose:   Convert Secs to DateTime
//
//  Parms:     buffer, secs
//  Returns:
//
void CVB_SecsToDateTime(CString *pclStr, u_int32 ulSecs)
{
   CLOCK_COUNT_INFO     stCount;
   CLOCK_DATE_TIME_INFO stDateTime;

   stCount.ulSeconds = ulSecs;
   //
   CLOCK_UtilCountToDateTime(&stCount, &stDateTime);
   //
   pclStr->Format(_T("%02d-%02d-%04d  %02d:%02d:%02d"),
                        stDateTime.ubDay,
                        stDateTime.ubMonth,
                        stDateTime.usYear+1900,
                        stDateTime.ubHour,
                        stDateTime.ubMin,
                        stDateTime.ubSec);
}

//
//  Function:  CVB_SecsToDate
//  Purpose:   Convert Secs to Date format "YYYYMMDD"
//
//  Parms:     buffer, secs
//  Returns:
//
void CVB_SecsToDate(CString *pclStr, u_int32 ulSecs)
{
   CLOCK_COUNT_INFO     stCount;
   CLOCK_DATE_TIME_INFO stDateTime;

   stCount.ulSeconds = ulSecs;
   //
   CLOCK_UtilCountToDateTime(&stCount, &stDateTime);
   //
   pclStr->Format(_T("%04d%02d%02d"),
                        stDateTime.usYear+1900,
                        stDateTime.ubMonth,
                        stDateTime.ubDay);
}

//
//  Function:  CVB_SecsToTime
//  Purpose:   Convert Secs to Time format "HHMM"
//
//  Parms:     buffer, secs
//  Returns:
//
void CVB_SecsToTime(CString *pclStr, u_int32 ulSecs)
{
   CLOCK_COUNT_INFO     stCount;
   CLOCK_DATE_TIME_INFO stDateTime;

   stCount.ulSeconds = ulSecs;
   //
   CLOCK_UtilCountToDateTime(&stCount, &stDateTime);
   //
   pclStr->Format(_T("%02d%02d"),
                        stDateTime.ubHour,
                        stDateTime.ubMin);
}

/**
 **  Name:         CVB_SkipWord
 **
 **  Description:  Skip the current word
 **
 **  Arguments:    buffer ptr
 **
 **  Returns:      buffer ptr
 **/
char *CVB_SkipWord(char *pcBuffer)
{
   while(*pcBuffer && (*pcBuffer != ' ') )
   {
      pcBuffer++;
   }
   while(*pcBuffer && (*pcBuffer == ' ') )
   {
      pcBuffer++;
   }
   return(pcBuffer);
}

/**
 **  Name:         CVB_SkipRedundantChars
 **
 **  Description:  Remove redundant chars from string
 **
 **  Arguments:    buffer ptr, char
 **
 **  Returns:      Number skipped
 **/
int CVB_SkipRedundantChars(char *pcBuffer, char cChar)
{
   BOOL  fDup = FALSE;
   int   iNr=0;

   while(pcBuffer && *pcBuffer)
   {
      if(*pcBuffer == cChar)
      {
         if(fDup == TRUE)
         {
            CVB_DeleteChar(pcBuffer);
            iNr++;
         }
         else
         {
            fDup = TRUE;
            pcBuffer++;
         }
      }
      else
      {
         fDup = FALSE;
         pcBuffer++;
      }
   }
   return(iNr);
}


