/*  (c) Copyright:  2009  CVB, Confidential Data  
**
**  $Workfile:          ClockApi.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            
**
**  Entries:            
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "CvbFiles.h"
#include "ClockApi.h"

/*  This array represents the number of days elapsed in one non-leap year
 *  at the beginning of each month (first value is a dummy, month 0).
 */
static u_int16 pusDaysMonth [] = {   0,   0,  31,  59,  90, 120, 151,
                                   181, 212, 243, 273, 304, 334, 365 };

static void    clock_Format            (u_int32, CLOCK_DATE_TIME_INFO*);
static void    clock_MJDtoYMD          (u_int32, u_int32*, u_int32*, u_int32*);
static void    clock_MJDtoDofW         (u_int32, u_int8*);
static void    clock_UTCtoHMS          (u_int32, u_int8*,  u_int8*,  u_int8 *);
static bool    clock_YMDtoMJD          (u_int16, u_int8,   u_int8,   u_int32*);
static bool    clock_IsLeapYear        (u_int16);

/******************************************************************************
 * Name:    CLOCK_UtilDaysInMonthGet
 *
 * Purpose: Get the number of days in a month
 *
 * Input:   ubMonth        month
 *          usYear         year
 *
 * Output:  pubDays        number of days in the given month
 *
 * Returns: CLOCK_CC_SUCCESS        success
 *          CLOCK_CC_FAILURE        unknown error
 *          CLOCK_CC_NULL_POINTER   null pointer error
 *
 * Note:
******************************************************************************/
CLOCK_CC CLOCK_UtilDaysInMonthGet(u_int8 ubMonth, u_int16 usYear, u_int8 *pubDays)
{
   CLOCK_CC tRet = CLOCK_CC_FAILURE;

   if (pubDays != NULL)
   {
      switch (ubMonth)
      {
         case 1:
         case 3:
         case 5:
         case 7:
         case 8:
         case 10:
         case 12:
            // 31 days
            *pubDays = 31;
            tRet = CLOCK_CC_SUCCESS;
            break;

         case 2:
            if (clock_IsLeapYear(usYear) == FALSE)
            {
               // 28 days
               *pubDays = 28;
            }
            else
            {
               // 29 days
               *pubDays = 29;
            }
            tRet = CLOCK_CC_SUCCESS;
            break;

         case 4:
         case 6:
         case 9:
         case 11:
            // 30 days
            *pubDays = 30;
            tRet = CLOCK_CC_SUCCESS;
            break;

         default:
            break;
      }
   }
   else
   {
      tRet = CLOCK_CC_NULL_POINTER;
   }
   return(tRet);
}

/******************************************************************************
 * Name:    CLOCK_UtilWeekdayGet
 *
 * Purpose: Get the weekday
 *
 * Input:   ubDay          day
 *          ubMonth        month
 *          usYear         year
 *
 * Output:  pubWeekday     weekday, (0 = Sunday, 1 = Monday, .., 6 = Saturday)
 *
 * Returns: CLOCK_CC_SUCCESS        success
 *          CLOCK_CC_FAILURE        unknown error
 *          CLOCK_CC_NULL_POINTER   null pointer error
 *
 * Note:
******************************************************************************/
CLOCK_CC CLOCK_UtilWeekdayGet(u_int8 ubDay,
                              u_int8 ubMonth,
                              u_int16 usYear,
                              u_int8 *pubWeekday)
{
   CLOCK_CC tRet = CLOCK_CC_FAILURE;
   u_int32  ulMJD;

   if (pubWeekday != NULL)
   {
      //
      if (clock_YMDtoMJD(usYear, ubMonth, ubDay, &ulMJD) != FALSE)
      {
         clock_MJDtoDofW(ulMJD, pubWeekday);

         tRet = CLOCK_CC_SUCCESS;
      }
   }
   else
   {
      tRet = CLOCK_CC_NULL_POINTER;
   }

   return tRet;
}

/******************************************************************************
 * Name:    CLOCK_UtilCountToDateTime
 *
 * Purpose: Convert seconds since 1970 to D&T structure
 *
 * Input:
 *
 * Output:
 *
 * Returns:
 *
 * Note:
******************************************************************************/
CLOCK_CC CLOCK_UtilCountToDateTime( CLOCK_COUNT_INFO *pstCount,
                                    CLOCK_DATE_TIME_INFO *pstDateTime)
{
   CLOCK_CC tRet = CLOCK_CC_SUCCESS;

   clock_Format(pstCount->ulSeconds, pstDateTime);

   return tRet;
}

/******************************************************************************
 * Name:    CLOCK_UtilDateTimeToCount
 *
 * Purpose: Convert a T&D structure to the number of secs since 1970
 *
 * Input:
 *
 * Output:
 *
 * Returns:
 *
 * Note:
******************************************************************************/
CLOCK_CC CLOCK_UtilDateTimeToCount( CLOCK_DATE_TIME_INFO *pstDateTime,
                                    CLOCK_COUNT_INFO *pstCount)
{
   u_int32  ulMJD;
   CLOCK_CC tRet = CLOCK_CC_FAILURE;

   if (pstCount != NULL && pstDateTime != NULL)
   {
      // Convert the formatted date to Modified Julian Date
      if ( clock_YMDtoMJD( (u_int16)(pstDateTime->usYear + 1900),
                           pstDateTime->ubMonth,
                           pstDateTime->ubDay,
                           &ulMJD ) != FALSE)
      {
         if ( ulMJD >= ES_MJD_1970_OFFSET )
         {
            ulMJD -= ES_MJD_1970_OFFSET;

            pstCount->ulSeconds = ( (ulMJD * ES_SECONDS_PER_DAY) +
                                    ((u_int32)pstDateTime->ubHour * ES_SECONDS_PER_HOUR) +
                                    ((u_int32)pstDateTime->ubMin * ES_SECONDS_PER_MINUTE) +
                                    pstDateTime->ubSec );
            pstCount->usMilliSeconds = 0;

            tRet = CLOCK_CC_SUCCESS;
         }
      }
   }
   else
   {
      tRet = CLOCK_CC_NULL_POINTER;
   }

   return tRet;
}

/*
______private_functions_____(){};
*/

/******************************************************************************
 * Name:    clock_MJDtoYMD
 *
 * Purpose: Converts MJD data/time format to YMD date/time format.
 *
 * Input:   ulMJD    - Modified Julian Date.
 *
 * Output:  pulYear  - Argument in which the year is set.
 *                     Argument value become related to ulMJD and is years since 1900.
 *          pulMonth - Argument in which the month is set.
 *                     Argument value becomes related to ulMJD, range: 1 - 12.
 *          pulDay   - Argument in which the day (related to ulMJD) is set.
 *                     Argument value becomes related to ulMJD, range: 1 - 31.
 *
 * Returns: ---
 *
 * Note:
******************************************************************************/
static void clock_MJDtoYMD(u_int32 ulMJD, u_int32 *pulYear, u_int32 *pulMonth, u_int32 *pulDay)
{
   u_int32 ulV1, ulV2, ulV3;
   u_int32 ulY1, ulM1;
   u_int32 ulK = 0;

   // calculate using integer formulas
   // year calculation from MPEG specification:
   //   Y' = int([MJD - 15078.2] / 365.25)
   // method: (in 10 radix)
   //   Y' = MJD*8 - 15078.2*8
   //   Y' = Y'/ (365.25 * 8)
   //
   //
   ulY1 = (u_int32)(((ulMJD << 3)   - 120626L)/2922);


   // month calculation from MPEG specification
   //   M' = int([MJD - 14956.1 - int(Y' * 365.25)] / 30.6001)
   // method: (in 10 radix)
   //   M' = MJD*8 - 14956.1*8 - int(Y' * 365.25*8)
   //   M' = (M'* 512) / (30.6001 * 512 * 8)
   //
   ulV1 = 2922L;

   ulM1 = (u_int32)((((ulMJD << 3) - 119649L - (0xFFFFFFF8l&(ulY1 * ulV1))) << 9) / 125338L);

   if ((ulM1 == 14) || (ulM1 == 15))
   {
        ulK = 1;
   }
   *pulYear = (u_int32)(ulY1 + ulK);

   *pulMonth = (u_int32)(ulM1 - 1 - ulK*12);


   //
   // day calculation from MPEG specification:
   //   D = MJD - 14956 - int(y' * 365.25) - int(m' * 30.6001)
   // method:  (in 10 radix)
   //   D = (MJD*16 - 14956*16 - int(y1*365.25*16))*256 - int(m1*30.6001*4096)
   //   D = D / 4096
   //      integer conversions are done by masking off all bits below the
   //      shift amount:     FFFF000 if term multiplied by 4096
   //                        FFFFFF0 if term multiplied by 16
   //                        FFFFFF8 if term multiplied by 8
   //
   ulV1 = 239296L;
   ulV2 = 5844L;
   ulV3 = 125338L;

   *pulDay = (u_int32)(((int32)((   ((int32) (ulMJD << 4)) - ulV1
                            -   ((int32)((0xFFFFFFF0L)&(ulY1*ulV2)))   ) << 8)
                            -   (int32)(0xFFFFF000L&(ulM1*ulV3))   ) >> 12);

}

/******************************************************************************
 * Name:    clock_MJDtoDofW
 *
 * Purpose: Converts MJD to the day of the week.
 *          Sunday = 0, ....., Saturday = 6.
 *
 * Input:   ulMJD       - Modified Julian Date.
 *
 * Output:  pubWeekDay  - Argument is set to the day of the week (related to ulMJD).
 *
 * Returns:
 *
 * Note:
******************************************************************************/
static void clock_MJDtoDofW(u_int32 ulMJD, u_int8 *pubWeekDay)
{
   //
   // day of week calculation from MPEG specification:
   //   WD = [ (MJD + 2)mod 7]
   // method:
   //   WD = ((MJD + 2) % 7)
   //   *wd = 0 (sunday) to 6 (saturday)
   //
   // @EH wrong: mon = 1, ..., sun = 7. *pulWeekDay = ((ulMJD + 2) % 7) + 1;
   *pubWeekDay = (u_int8)((ulMJD + 3) % 7);
}

/******************************************************************************
 * Name:    clock_UTCtoHMS
 *
 * Purpose: Convertes an UTC represented time to HMS represented time.
 *
 * Input:   ulUTC - Universal Time Co-ordinated
 *
 * Output:  pubHour  - Number of Hours (0 .. 23)
 *          pubMin   - Number of Minutes (0 .. 59)
 *          pubSec   - Number of Seconds (0 .. 59)
 *
 * Returns: ---
 *
 * Note:
******************************************************************************/
static void clock_UTCtoHMS(u_int32 ulUTC, u_int8 *pubHour, u_int8 *pubMin, u_int8 *pubSec)
{
   u_int32 ulCurrentSeconds = ulUTC;

   // calculate total seconds into usable parts
   ulCurrentSeconds  = (u_int32)(ulCurrentSeconds % ES_SECONDS_PER_DAY    );
   *pubHour          = (u_int8 )(ulCurrentSeconds / ES_SECONDS_PER_HOUR   );
   ulCurrentSeconds  = (u_int32)(ulCurrentSeconds % ES_SECONDS_PER_HOUR   );
   *pubMin           = (u_int8 )(ulCurrentSeconds / ES_SECONDS_PER_MINUTE );
   *pubSec           = (u_int8 )(ulCurrentSeconds % ES_SECONDS_PER_MINUTE );
}

/******************************************************************************
 * Name:    clock_Format
 *
 * Purpose: This function will convert a date/time specified in seconds since
 *          1-1-1970 to a formatted date/time.
 *
 * Input:   ulSeconds - Date specified in seconds since 1-1-1970
 *
 * Output:  pstDayTime - Formatted date/time
 *
 * Returns: ---
 *
 * Note:
******************************************************************************/
static void clock_Format(u_int32 ulSeconds, CLOCK_DATE_TIME_INFO *pstDateTime)
{
   u_int32 ulMJD;
   u_int32 ulUTC;
   u_int32 ulYear;
   u_int32 ulMonth;
   u_int32 ulDay;

   ulMJD = ulSeconds / ES_SECONDS_PER_DAY;
   ulMJD += ES_MJD_1970_OFFSET;
   ulUTC = ulSeconds % ES_SECONDS_PER_DAY;

   clock_MJDtoYMD(ulMJD, &ulYear, &ulMonth, &ulDay);
   clock_MJDtoDofW(ulMJD, &pstDateTime->ubWeekDay);
   clock_UTCtoHMS(ulUTC, &pstDateTime->ubHour, &pstDateTime->ubMin, &pstDateTime->ubSec);

   pstDateTime->ubMonth = (u_int8)ulMonth;
   pstDateTime->ubDay = (u_int8)ulDay;
   pstDateTime->usYear = (u_int16)ulYear;// + 1900;
}

/******************************************************************************
 * Name:    clock_YMDtoMJD
 *
 * Purpose: Converts year, month and day to MJD.
 *
 * Input:   usYear   - Year.
 *          ubMonth  - Month.
 *          ubDay    - Day.
 *
 * Output:  pulMJD   - Argument in which the Modified Julian Date is set.
 *
 * Returns: TRUE  -  MJD calculation correct
 *          FALSE -  MJD doesn't exist for this date
 *
 * Note:
******************************************************************************/
static bool clock_YMDtoMJD(u_int16 usYear, u_int8 ubMonth, u_int8 ubDay, u_int32 *pulMJD)
{
   bool fResult = FALSE, fCalcMJD = TRUE;

   if ( pulMJD != NULL )
   {
      // Check if the date exist in MJD
      if ( usYear < 1858 )
      {
         fCalcMJD = FALSE;
      }
      else
      {
         // If date does exist and the year is 1858, then it's a special case
         // because MJD starts at 17/11/1858
         if ( usYear == 1858 )
         {
            fCalcMJD = FALSE;
            if ( ubMonth == 11 )
            {
               if ( ubDay > 16 )
               {
                  *pulMJD = (u_int32)ubDay - (u_int32)17;
                  fResult = TRUE;
               }
            }
            if ( ubMonth == 12 )
            {
               *pulMJD = (u_int32)13 + (u_int32)ubDay;
               fResult = TRUE;
            }
         }
      }

      if ( fCalcMJD != FALSE )
      {
         *pulMJD = (((u_int32)usYear - (u_int32)1859) * (u_int32)365)
                   + (u_int32)pusDaysMonth[ ubMonth ]
                   + (u_int32)ubDay;                                    // regular days
         *pulMJD += ((u_int32)usYear - (u_int32)1857) / (u_int32)4;     // + leap days

         if ( (ubMonth > 2) && ((usYear % 4) == 0) )
         {
            *pulMJD += 1; // Add this year's leapday
         }
         if ( (usYear > 1900) || ((usYear == 1900) && (ubMonth > 2 )) )
         {
            *pulMJD -= 1; // Remove a day, because 1900 wasn't a leapyear
         }

         *pulMJD += 44; // Add the 44 resulting days of year
         fResult = TRUE;
      }
   }
   return fResult;
}

/******************************************************************************
 * Name:    clock_IsLeapYear
 *
 * Purpose:
 *
 * Input:
 *
 * Output:
 *
 * Returns:
 *
 * Note:
******************************************************************************/
static bool clock_IsLeapYear(u_int16 usYear)
{
   return ( (usYear % 4 == 0) &&
            ((usYear % 100 != 0) || (usYear % 400 == 0))
          );
}
