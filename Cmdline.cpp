/*  (c) Copyright:  2011  CVB, Confidential Data
**
**  $Workfile:          Cmdline.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Defines the Commandline class
**
**
**
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       16 Aug 2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "Cmdline.h"


//
// Need to declare the static CMyCommandLineInfo members
//
CMDLINE  CMyCommandLineInfo::stCmdLine[52];              // all a..z and A..Z switches
CString  CMyCommandLineInfo::clStrCmdLineAll;            // Command line

/* ====== Functions separator ===========================================
void ____Command_Line____(){}
=========================================================================*/

//
//  Function:  CMyCommandLineInfo
//  Purpose:   Constructor of the Commandline info class
//
//  Parms:
//  Returns:
//
CMyCommandLineInfo::CMyCommandLineInfo(void)
{
   CCommandLineInfo();
   //
   iInstance++;
   //
   fSuccess  = TRUE;
}

//
//  Function:  Cleanup
//  Purpose:   Cleanup the Cmdline data
//
//  Parms:
//  Returns:
//
void CMyCommandLineInfo::Cleanup(void)
{
   CString *pclStr;

   for(int i=0; i<52; i++)
   {
      pclStr = stCmdLine[i].pclValue;
      if(pclStr) delete(pclStr);
   }
}

//
//  Function:  GetCommandline
//  Purpose:   Get the complete commandline
//
//  Parms:     CString *
//  Returns:
//
void CMyCommandLineInfo::GetCommandline(CString *pclStr)
{
   *pclStr = clStrCmdLineAll;
}

//
//  Function:  GetParmLength
//  Purpose:   Get the size of the current commandline entry (terminated by ',', space or /0)
//
//  Parms:
//  Returns:
//
int CMyCommandLineInfo::GetParmLength(const char *pcParm)
{
   int   iSize=0;
   BOOL  fCounting=TRUE;

   while(fCounting)
   {
      switch(*pcParm++)
      {
         default:
            iSize++;
            break;

         case ' ':
         case ',':
         case 0:
            iSize++;
            fCounting=FALSE;
            break;
      }
   }
   return(iSize);
}

//
//  Function:  GetSwitchValue
//  Purpose:   Get the value of a certain cmd line switch
//
//  Parms:     switch a..z or A..Z, CString *data
//  Returns:   TRUE if data available
//
BOOL CMyCommandLineInfo::GetSwitchValue(char cSwitch, CString *pclSwitch)
{
   BOOL     fCc=FALSE;
   CString *pclStr;
   int      iSwitch=MakeIndex(cSwitch);

   if(iSwitch >= 0)
   {
      pclStr = stCmdLine[iSwitch].pclValue;
      if(pclStr)
      {
         *pclSwitch = *pclStr;
         fCc        = TRUE;
      }
   }
   return(fCc);
}

//
//  Function:  IsSwitchPresent
//  Purpose:   Get the boolean of the presence of a cmd line switch
//
//  Parms:     switch a..z or A..Z
//  Returns:   TRUE/FALSE
//
BOOL CMyCommandLineInfo::IsSwitchPresent(char cSwitch)
{
   BOOL  fCc=FALSE;
   int   iSwitch=MakeIndex(cSwitch);

   if(iSwitch >= 0)
   {
      fCc = stCmdLine[iSwitch].fPresent;
   }
   return(fCc);
}

//
//  Function:  IsSwitchOn
//  Purpose:   Get the value of a certain cmd line switch
//
//  Parms:     switch a..z or A..Z
//  Returns:   TRUE/FALSE
//
BOOL CMyCommandLineInfo::IsSwitchOn(char cSwitch)
{
   BOOL  fCc=FALSE;
   int   iSwitch=MakeIndex(cSwitch);

   if(iSwitch >= 0)
   {
      fCc = stCmdLine[iSwitch].fOn;
   }
   return(fCc);
}

//
//  Function:  MakeIndex
//  Purpose:   Rework input cmd to index
//
//  Parms:     A..Z or a..z
//  Returns:   0..51 or -1
//
int CMyCommandLineInfo::MakeIndex(char cParm)
{
   int   iIdx=-1;

   if(cParm >= 'A') iIdx = (int)(cParm - 'A');
   if(cParm <= 'Z') return(iIdx);
   //
   if(cParm <  'a') return(-1);
   //
   if(cParm <= 'z') return(iIdx-6);
   //
   return(-1);
}

//
//  Function:  ParseParam
//  Purpose:   Parser for the command line info
//             Cmdline example:
//             > bla.exe -h +q /d=D:\Folder1\Folder2 +t=33 +a=50
//             This results in 5 successive calls to ParseParm
//
//             Call  fSwitch  bLast pcParm
//             --------------------------------------------------------------
//              1       1       0   "h"
//              2       0       0   "+q"
//              3       1       0   "d=D:\Folder1\Folder2"
//              4       0       0   "+t=33"
//              5       0       1   "+l=50"
//
//  Parms:
//  Returns:
//
void CMyCommandLineInfo::ParseParam(LPCTSTR pcParms, BOOL fSwitch, BOOL bLast)
{
   BOOL  fContinue = TRUE;
   BOOL  fParam    = FALSE;
   int   iParm, iLen, i=0;

   memset(stCmdLine, 0, sizeof(stCmdLine));
   iLen = lstrlen(pcParms);

   while(fContinue)
   {
      switch(pcParms[i])
      {
         case _T('-'):
            fParam = FALSE;
            i++;
            break;

         case _T('/'):
         case _T('+'):
            fParam = TRUE;
            i++;
            break;

         default:
            iParm = MakeIndex(pcParms[i]);
            if(iParm >=0)
            {
               stCmdLine[iParm].fPresent = TRUE;
               stCmdLine[iParm].fOn      = fParam;
               if(pcParms[i+1] == '=')
               {
                  stCmdLine[iParm].pclValue = new(CString);
                  stCmdLine[iParm].pclValue->Format(_T("%s"), &pcParms[i+2]);
               }
            }
            i += GetParmLength(&pcParms[i]);
            break;
      }
      if(i >= iLen) fContinue = FALSE;
   }
   if(fSwitch) clStrCmdLineAll += " -";
   else        clStrCmdLineAll += " ";
   //
   clStrCmdLineAll += pcParms;
}

