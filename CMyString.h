/*  (c) Copyright:  2012  CVB, Confidential Data
**
**  $Workfile:          Pstring.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            CString extensions
**
**
**
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       31 Mar 2012
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_PSTRING_H_)
#define _PSTRING_H_


class CMyString : public CString
{
public:
            CMyString      ();
   virtual ~CMyString      ();
CMyString&  CMyString::operator= (const CMyString&);
CMyString&  CMyString::operator= (const CString&);
CMyString&  CMyString::operator= (const char *);

   int      GetCharCount   (char);
   void     GetDelimiter   (CString *);
   BOOL     GetWord        (CString *, int);
   BOOL     GetWord        (CString *, int, int);
   BOOL     GetWord        (CString *, CString *, int);
   BOOL     GetWord        (CString *, CString *, int, int);
   int      GetWordCount   (void);
   int      GetWordCount   (int);
   int      GetWordCount   (CString *);
   int      GetWordCount   (CString *, int);
   int      GetLetterCount (int);
   int      GetLetterCount (CString *, int);
   int      GetLetterCount (int, int);
   int      GetLetterCount (CString *, int, int);
   int      RemoveAll      (char *);
   void     SetDelimiter   (CString *);
   void     SetDelimiter   (char *);
   int      Substitute     (char *, char);
   //
private:
   //
   CString  clDelimiter;

public:
};

#endif   //_PSTRING_H_
