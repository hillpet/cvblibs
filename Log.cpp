/*  Copyright 2011-2012 Patrn.nl
**
**  $Workfile:   Log.cpp $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:     Log class implementation
**
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       10 Mar 2012
**
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "NvmStorage.h"
#include "CvbFiles.h"
#include "Log.h"

//
// Static members
//
BOOL           CLog::fAllEnabled;
int            CLog::iInstance;
//
CStdioFile    *CLog::pclLogFile[];
BOOL           CLog::fLogOpen[];
BOOL           CLog::fLogEnabled[];
BOOL           CLog::fLogAppend[];
CString       *CLog::pclLogPath[];

extern const char *pcCommentLines = "--------------------------------------------------------------------------------";
//
//  Function:  CLog()
//  Purpose:   constructor
//  Parms:
//  Returns:
//
CLog::CLog()
{
   pclNvm = new(CNvmStorage);
   //
   if(iInstance == 0)
   {
      for(int i=0; i<MAX_LOGS; i++)
      {
         fLogOpen[i]    = FALSE;
         fLogEnabled[i] = FALSE;
         fLogAppend[i]  = FALSE;
         pclLogFile[i]  = NULL;
         pclLogPath[i]  = NULL;
      }
      fAllEnabled       = FALSE;
   }
   //
   iInstance++;
}

//
//  Function:  CLog
//  Purpose:   Destructor
//  Parms:
//  Returns:
//
//  Note:
//
//
CLog::~CLog()
{
   delete(pclNvm);

   iInstance--;
   if(iInstance == 0)
   {
      for(int i=0; i<MAX_LOGS; i++)
      {
         if(pclLogFile[i])  delete(pclLogFile[i]);
         if(pclLogPath[i])  delete(pclLogPath[i]);
      }
   }
}

/* ======   Local   Functions separator ===========================================
void ____Public_Methods____(){}
==============================================================================*/

//
//  Function:  LOG_Enable
//  Purpose:   Enable a logfile
//  Parms:     Handle, TRUE/FALSE
//  Returns:
//
void CLog::LOG_Enable(u_int16 usHandle, BOOL fEnable)
{
   if(usHandle<MAX_LOGS)
   {
      fLogEnabled[usHandle] = fEnable;
   }
}

//
//  Function:  LOG_NvmEnable
//  Purpose:   Enable a logfile
//  Parms:     Handle, NVM Storage location
//  Returns:
//
void CLog::LOG_NvmEnable(u_int16 usHandle, int iNvmId)
{
   if(usHandle<MAX_LOGS)
   {
      pclNvm->NvmGet(iNvmId, &fLogEnabled[usHandle]);
   }
}

//
//  Function:  LOG_EnableAll
//  Purpose:   Enable all logfiles
//  Parms:     TRUE/FALSE
//  Returns:
//
void CLog::LOG_EnableAll(BOOL fEnable)
{
   fAllEnabled = fEnable;
}

//
//  Function:  LOG_NvmEnableAll
//  Purpose:   Enable all logfiles
//  Parms:     NVM Storage location
//  Returns:
//
void CLog::LOG_NvmEnableAll(int iNvmId)
{
   pclNvm->NvmGet(iNvmId, &fAllEnabled);
}

//
//  Function:  LOG_Register
//  Purpose:   Register a logfiles
//  Parms:     Log Handle, pathname, Append YesNo
//  Returns:   TRUE if OKee
//
BOOL CLog::LOG_Register(u_int16 usHandle, char *pcPathname, BOOL fAppend)
{
   BOOL fCc = FALSE;

   if(usHandle<MAX_LOGS)
   {
      if(pclLogFile[usHandle] == NULL)
      {
         //
         // Allocate a CFile   for the file
         // Allocate a CString for the log pathname
         //
         pclLogFile[usHandle]  = new(CStdioFile);
         pclLogPath[usHandle]  = new(CString);
         *pclLogPath[usHandle] = *pcPathname;
         fLogOpen[usHandle]    = FALSE;
         fLogEnabled[usHandle] = FALSE;
         fLogAppend[usHandle]  = fAppend;
         fCc                   = TRUE;
      }
   }
   return(fCc);
}

//
//  Function:  LOG_Register
//  Purpose:   Register a logfiles
//  Parms:     Log Handle, pathname, Append YesNo
//  Returns:   TRUE if OKee
//
BOOL CLog::LOG_Register(u_int16 usHandle, CString *pclPathname, BOOL fAppend)
{
   BOOL fCc = FALSE;

   if(usHandle<MAX_LOGS)
   {
      if(pclLogFile[usHandle] == NULL)
      {
         //
         // Allocate a CFile   for the file
         // Allocate a CString for the log pathname
         //
         pclLogFile[usHandle]  = new(CStdioFile);
         pclLogPath[usHandle]  = new(CString);
         *pclLogPath[usHandle] = *pclPathname;
         fLogOpen[usHandle]    = FALSE;
         fLogEnabled[usHandle] = FALSE;
         fLogAppend[usHandle]  = fAppend;
         fCc                   = TRUE;
      }
   }
   return(fCc);
}

//
//  Function:  LOG_Unregister
//  Purpose:   Unregister logfiles
//  Parms:     Log ID
//  Returns:
//
void CLog::LOG_Unregister(u_int16 usHandle)
{
   if(usHandle<MAX_LOGS)
   {
      //
      // Make sure to close the file first
      //
      LOG_Write(usHandle, (char *) NULL, 0);
      //
      if(pclLogFile[usHandle] != NULL)
      {
         delete(pclLogFile[usHandle]);
         pclLogFile[usHandle] = NULL; 
      }
      if(pclLogPath[usHandle] != NULL)
      {
         delete(pclLogPath[usHandle]);
         pclLogPath[usHandle] = NULL; 
      }
   }
}

//
//  Function:  LOG_Write
//  Purpose:   Write a record to a log file
//
//  Parms:     LogHandle, CString, mode
//  Returns:   TRUE if OKee
//
//    modes:
//             LOG_TIMESTAMP
//             LOG_CRLF
//
BOOL CLog::LOG_Write(u_int16 usHandle, CString *pclStr, int iMode)
{
   return(LOG_Write(usHandle, pclStr->GetBuffer(), iMode));
}

//
//  Function:  LOG_Write
//  Purpose:   Write a record to a log file
//
//  Parms:     LogHandle, Buffer, mode
//  Returns:   TRUE if OKee
//
//    modes:
//             LOG_TIMESTAMP
//             LOG_CRLF
//
BOOL CLog::LOG_Write(u_int16 usHandle, char *pcRecord, int iMode)
{
   BOOL     fWrite;
   CString  clStr;

   if(usHandle >= MAX_LOGS)                                    return(FALSE);
   if((fLogEnabled[usHandle]==FALSE) || (fAllEnabled==FALSE) ) return(TRUE);

   //
   // Check if this logfile needs to close.
   //
   if( (fLogOpen[usHandle] == TRUE) && (pcRecord == NULL) )
   {
      pclLogFile[usHandle]->Close();
      fLogOpen[usHandle] = FALSE;
      return(TRUE);
   }
   //
   // Check if this logfile is open. If not, do it now.
   //
   if(fLogOpen[usHandle] == FALSE)
   {
      fWrite = pclLogFile[usHandle]->Open(pclLogPath[usHandle]->GetBuffer(), CFile::modeWrite|CFile::modeCreate, NULL);
      if(fWrite)
      {
         if(fLogAppend[usHandle]) pclLogFile[usHandle]->SeekToEnd();
         fLogOpen[usHandle] = TRUE;
      }
   }
   //
   // Write a string to the file
   //
   if(fLogOpen[usHandle] == TRUE)
   {
      if(iMode & LOG_TIMESTAMP)
      {
         clStr  = "[";
         clStr += CVB_GetTimeDateStamp();
         clStr += "]=";
         pclLogFile[usHandle]->WriteString(clStr);
      }
      pclLogFile[usHandle]->WriteString(pcRecord);
      //
      if(iMode & LOG_CRLF)
      {
         pclLogFile[usHandle]->WriteString("\n");
      }
   }
   return(fLogOpen[usHandle]);
}

//
//  Function:  LOG_Write
//  Purpose:   Write a record plus bcd-data to a log file
//
//  Parms:     LogHandle, Buffer, value, mode
//  Returns:   TRUE if OKee
//
//    modes:
//             LOG_TIMESTAMP
//             LOG_CRLF
//
BOOL CLog::LOG_Write(u_int16 usHandle, char *pcRecord, int iValue, int iMode)
{
   CString  clStr;

   clStr.Format(_T("%s%d"), pcRecord, iValue);
   return( LOG_Write(usHandle, &clStr, iMode) );
}

//
//  Function:  LOG_Write
//  Purpose:   Write a record plus bcd-data to a log file
//
//  Parms:     LogHandle, Buffer, value, mode
//  Returns:   TRUE if OKee
//
//    modes:
//             LOG_TIMESTAMP
//             LOG_CRLF
//
BOOL CLog::LOG_Write(u_int16 usHandle, CString *pclRecord, int iValue, int iMode)
{
   CString  clStr;

   clStr.Format(_T("%s%d"), pclRecord, iValue);
   return( LOG_Write(usHandle, &clStr, iMode) );
}

//
//  Function:  LOG_Write
//  Purpose:   Write a record plus hex-data to a log file
//
//  Parms:     LogHandle, Buffer, value, mode
//  Returns:   TRUE if OKee
//
//    modes:
//             LOG_TIMESTAMP
//             LOG_CRLF
//
BOOL CLog::LOG_Write(u_int16 usHandle, char *pcRecord, u_int32 ulValue, int iMode)
{
   CString  clStr;

   clStr.Format(_T("%s%x"), pcRecord, ulValue);
   return( LOG_Write(usHandle, &clStr, iMode) );
}

//
//  Function:  LOG_Write
//  Purpose:   Write a record plus hex-data to a log file
//
//  Parms:     LogHandle, Buffer, value, mode
//  Returns:   TRUE if OKee
//
//    modes:
//             LOG_TIMESTAMP
//             LOG_CRLF
//
BOOL CLog::LOG_Write(u_int16 usHandle, CString *pclRecord, u_int32 ulValue, int iMode)
{
   CString  clStr;

   clStr.Format(_T("%s%x"), pclRecord, ulValue);
   return( LOG_Write(usHandle, &clStr, iMode) );
}




