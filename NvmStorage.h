/*  (c) Copyright:  2011 PATRN.NL
**
**  $Workfile:  NvmStorage.h $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Header file for the *.cpp
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       06 Jun 2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(AFX_NVMSTORAGE_H_)
#define AFX_NVMSTORAGE_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//
// Include the NVM storage members
//
#include "CvbFiles.h"

//
// The Class for handling permanent private storage of application data in the document
//
class CNvmStorage : public CObject
{
public:
             CNvmStorage();
             CNvmStorage(int);
    virtual ~CNvmStorage();

public:
   void     NvmPut            (int, CString);
   void     NvmPut            (int, BOOL);
   void     NvmPut            (int, u_int8);
   void     NvmPut            (int, char *);
   void     NvmPut            (int, u_int8 *, int);

   BOOL     NvmGet            (int, CString *);
   BOOL     NvmGet            (int, u_int8 *);
   BOOL     NvmGet            (int, char *);
   BOOL     NvmGet            (int, u_int8 *, int);
   BOOL     NvmGet            (int, BOOL *);
   int      NvmGetSize        (int);

   int      NvmCheckArchive   (CString *);
   BOOL     NvmWrite          (CString *);
   BOOL     NvmRead           (CString *);

private:
   void     Serialize         (CArchive &);
   static   int      iInstance;
   static   CString *clNVstore;
   static   int      iNumWnvms;

DECLARE_SERIAL(CNvmStorage)
};

#endif // !defined(AFX_NVMSTORAGE_H_)
