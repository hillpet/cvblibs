/*  (C) Copyright 2012 Patrn.nl
**
**  $Workfile:  Log.h $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Define a generic Log class
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       10 Mar 2012
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_LOG_H_)
#define _LOG_H_

//
// Defines
//
#define  MAX_LOGS          8
//
#define  LOG_NONE          0x0000
#define  LOG_TIMESTAMP     0x0001
#define  LOG_CRLF          0x0002
//
extern const char *pcCommentLines;

//
// CLog
//
class CLog
{
public:
            CLog();
   virtual ~CLog();

public:
   void     LOG_Enable                    (u_int16, BOOL);
   void     LOG_NvmEnable                 (u_int16, int);
   void     LOG_EnableAll                 (BOOL);
   void     LOG_NvmEnableAll              (int);
   BOOL     LOG_Register                  (u_int16, char *, BOOL);
   BOOL     LOG_Register                  (u_int16, CString *, BOOL);
   void     LOG_Unregister                (u_int16);
   BOOL     LOG_Write                     (u_int16, char *, int);
   BOOL     LOG_Write                     (u_int16, char *, int, int);
   BOOL     LOG_Write                     (u_int16, char *, u_int32, int);
   BOOL     LOG_Write                     (u_int16, CString *, int);
   BOOL     LOG_Write                     (u_int16, CString *, int, int);
   BOOL     LOG_Write                     (u_int16, CString *, u_int32, int);

public:
   static int     iInstance;

private:
   CNvmStorage   *pclNvm;
   //
   // Static for all LOG users
   //
   static BOOL           fAllEnabled;
   //
   static CStdioFile    *pclLogFile  [MAX_LOGS];
   static BOOL           fLogOpen    [MAX_LOGS];
   static BOOL           fLogEnabled [MAX_LOGS];
   static BOOL           fLogAppend  [MAX_LOGS];
   static CString       *pclLogPath  [MAX_LOGS];
   //
};


#endif // !defined(_LOG_H_)
