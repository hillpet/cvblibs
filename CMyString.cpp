/*  (c) Copyright:  2012  CVB, Confidential Data
**
**  $Workfile:          Pstring.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Extensions to CString
**
**
**
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       31 Mar 2012
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "CmyString.h"


/* ====== Functions separator ===========================================
void ____Global_Functions____(){}
=========================================================================*/

//
// Default delimiter
//
const char *pcWordDelims = " ,.-?!\t\n\r";

//
//  Function:  CMyString
//  Purpose:   Constructor
//
//  Parms:     void
//  Returns:   void
//
CMyString::CMyString()
{
   clDelimiter = pcWordDelims;
}

//
//  Function:  CMyString
//  Purpose:   Destructor
//
//  Parms:     void
//  Returns:   void
//
CMyString::~CMyString()
{
}

//
//  Function:  operator =
//  Purpose:   Copy class data
//  Parms:     Source data
//  Returns:   Copied class
//
//
CMyString& CMyString::operator=(const CMyString& clStr)
{
   int   iLen = clStr.GetLength();
   char *pcX = GetBuffer(iLen+1);

   if (this != &clStr)
   {
      strcpy(pcX, clStr);
      ReleaseBuffer(-1);
   }
   return(*this);
}

//
//  Function:  operator =
//  Purpose:   Copy class data
//  Parms:     Source data
//  Returns:   Copied class
//
//
CMyString& CMyString::operator=(const CString& clStr)
{
   int   iLen = clStr.GetLength();
   char *pcX = GetBuffer(iLen+1);

   if (this != &clStr)
   {
      strcpy(pcX, clStr);
      ReleaseBuffer(-1);
   }
   return(*this);
}

//
//  Function:  operator =
//  Purpose:   Copy class data
//  Parms:     Source data
//  Returns:   Copied class
//
//
CMyString& CMyString::operator=(const char *pcStr)
{
   int   iLen = (int) strlen(pcStr);
   char *pcX = GetBuffer(iLen+1);

   strcpy(pcX, pcStr);
   ReleaseBuffer(-1);

   return(*this);
}

//
//  Function:  GetCharCount
//  Purpose:   Count the number of occurences of a char
//
//  Parms:     Char
//  Returns:   Number of occurences of the char
//
int CMyString::GetCharCount(char cChar)
{
   int   iNr=0;
   char  cBuf;
   char *pcBuffer = GetBuffer();

   while( (cBuf = *pcBuffer++) != 0)
   {
      if(cBuf == cChar) iNr++;
   }
   return(iNr);
}

//
//  Function:  GetDelimiter
//  Purpose:   Return the current delimiter
//
//  Parms:     Delimiter
//  Returns:   void
//
void CMyString::GetDelimiter(CString *pclDelim)
{
   *pclDelim = clDelimiter;
}

//
//  Function:  GetWord
//  Purpose:   Get a single word from a CString
//
//  Parms:     Word, word number 1...?
//  Returns:   OKee if found
//
//             "01 - Title title title - Author A. A. Author"
//
BOOL CMyString::GetWord(CString *pclWord, int iWord)
{
   return( GetWord(this, pclWord, iWord, 1) );
}

//
//  Function:  GetWord
//  Purpose:   Get a single word from a CString
//
//  Parms:     CString, Word, word number 1...?
//  Returns:   OKee if found
//
//             "01 - Title title title - Author A. A. Author"
//
BOOL CMyString::GetWord(CString *pclWord, int iWord, int iMinLen)
{
   return( GetWord(this, pclWord, iWord, iMinLen) );
}

//
//  Function:  GetWord
//  Purpose:   Get a single word from a CString
//
//  Parms:     Source string, Word, word number 1...?
//  Returns:   OKee if found
//
//             "01 - Title title title - Author A. A. Author"
//
BOOL CMyString::GetWord(CString *pclRecord, CString *pclWord, int iWord)
{
   return( GetWord(pclRecord, pclWord, iWord, 1) );
}

//
//  Function:  GetWord
//  Purpose:   Get a single word from a CString
//
//  Parms:     Source string, Word, word number 1...?, min world length
//  Returns:   OKee if found
//
//             "01 - Title title title - Author A. A. Author"
//
BOOL CMyString::GetWord(CString *pclRecord, CString *pclWord, int iWord, int iMinLen)
{
   BOOL     fFound=FALSE;
   int      iIdxE, iIdxS=0, iLen, iCount=0;
   CString  clStr;

   iLen = pclRecord->GetLength();
   //
   do
   {
      iIdxE = iLen - iIdxS;
      clStr = pclRecord->Right(iIdxE);
      iIdxE = clStr.FindOneOf(clDelimiter);
      if(iIdxE == -1)
      {
         // Remaining word is NULL terminated
         if(clStr.GetLength() >= iMinLen)
         {
            iCount++;
            if(iCount == iWord) 
            {
               *pclWord = clStr;
               fFound   = TRUE;
            }
         }
         break;
      }
      if(iIdxE)
      {
         if(iIdxE >= iMinLen)
         {
            iCount++;
            if(iCount == iWord) 
            {
               *pclWord = clStr.Left(iIdxE);
               fFound   = TRUE;
               break;
            }
         }
         else
         {
            //
            // Word id too short: skip it
            //
         }
      }
      iIdxS += iIdxE+1;
   }
   while(iIdxE < iLen);
   //
   return(fFound);
}

//
//  Function:  GetLetterCount
//  Purpose:   Return the number of letters in all words
//
//  Parms:     Num words
//  Returns:   Count
//
//             "01 - Title title title - Author A. A. Author"
//
int CMyString::GetLetterCount(int iNumWords)
{
   return(GetLetterCount(this, iNumWords, 1) );
}

//
//  Function:  GetLetterCount
//  Purpose:   Return the number of letters in all words
//
//  Parms:     String, Num words
//  Returns:   Count
//
//             "01 - Title title title - Author A. A. Author"
//
int CMyString::GetLetterCount(CString *pclStr, int iNumWords)
{
   return(GetLetterCount(pclStr, iNumWords, 1) );
}

//
//  Function:  GetLetterCount
//  Purpose:   Return the number of letters in all words
//
//  Parms:     Num words
//  Returns:   Count
//
//             "01 - Title title title - Author A. A. Author"
//
int CMyString::GetLetterCount(int iNumWords, int iMinLen)
{
   return(GetLetterCount(this, iNumWords, iMinLen) );
}

//
//  Function:  GetLetterCount
//  Purpose:   Return the number of letters in all words
//
//  Parms:     String, Num words, min word length
//  Returns:   Count
//
//             "01 - Title title title - Author A. A. Author"
//
int CMyString::GetLetterCount(CString *pclStr, int iNumWords, int iMinLen)
{
   int      iNumLetters=0;
   CString  clWord;

   for(int i=1; i<=iNumWords; i++)
   {
      if( GetWord(pclStr, &clWord, i, iMinLen) ) iNumLetters += clWord.GetLength();
      else break;
   }
   return(iNumLetters);
}

//
//  Function:  GetWordCount
//  Purpose:   Return the number of words in aCString
//
//  Parms:     
//  Returns:   Count
//
//             "01 - Title title title - Author A. A. Author"
//    count =  8
//
int CMyString::GetWordCount(void)
{
   return( GetWordCount(this, 1) );
}

//
//  Function:  GetWordCount
//  Purpose:   Return the number of words in aCString
//
//  Parms:     CString
//  Returns:   Count
//
//             "01 - Title title title - Author A. A. Author"
//    count =  8
//
int CMyString::GetWordCount(CString *pclRecord)
{
   return( GetWordCount(pclRecord, 1) );
}

//
//  Function:  GetWordCount
//  Purpose:   Return the number of words in aCString
//
//  Parms:     Min wordlength
//  Returns:   Count
//
//             "01 - Title title title - Author A. A. Author"
//    count =  8
//
int CMyString::GetWordCount(int iMinLen)
{
   return( GetWordCount(this, iMinLen) );
}

//
//  Function:  GetWordCount
//  Purpose:   Return the number of words in aCString
//
//  Parms:     String, min-wordlength
//  Returns:   Count
//
//             "01 - Title title title - Author A. A. Author"
//    count =  8
//
int CMyString::GetWordCount(CString *pclRecord, int iMinLen)
{
   int      iLen, iIdxE, iIdxS=0, iCount=0;
   CString  clStr;

   iIdxE  = pclRecord->GetLength();
   //
   do
   {
      iLen  = iIdxS;
      clStr = pclRecord->Right(iIdxE);
      iIdxS = clStr.FindOneOf(clDelimiter);
      if(iIdxS == -1) 
      {
         // Remaining word is NULL terminated
         if(clStr.GetLength() >= iMinLen) iCount++;
         break;
      }
      if(iIdxS)
      {
         if(iIdxS >= iMinLen) iCount++;
      }
      iLen   = iIdxS+1;
      iIdxE -= iLen;
   }
   while(iIdxE > 0);
   //
   return(iCount);
}

//
//  Function:  RemoveAll
//  Purpose:   Remove (delete) all occurences in a set of chars
//
//  Parms:     Removal list
//  Returns:   Number of replacements
//
int CMyString::RemoveAll(char *pcList)
{
   int   y=0, iNum=0;

   while(pcList[y])
   {
      iNum += Remove(pcList[y]);
      y++;
   }
   return(iNum);
}

//
//  Function:  SetDelimiter
//  Purpose:   Set the current delimiter
//
//  Parms:     Delimiter
//  Returns:   void
//
void CMyString::SetDelimiter(CString *pclDelim)
{
   if(pclDelim)   clDelimiter = *pclDelim;
   else           clDelimiter = pcWordDelims;
}

//
//  Function:  SetDelimiter
//  Purpose:   Set the current delimiter
//
//  Parms:     Delimiter
//  Returns:   void
//
void CMyString::SetDelimiter(char *pcDelim)
{
   if(pcDelim)   clDelimiter = pcDelim;
   else          clDelimiter = pcWordDelims;
}

//
//  Function:  Substitute
//  Purpose:   Substitute a set of chars
//
//  Parms:     Substitute list, substitute into char
//  Returns:   Number of replacements
//
int CMyString::Substitute(char *pcList, char cChar)
{
   int   x, y, iNum=0, iLen=this->GetLength();

   for(x=0; x<iLen; x++)
   {
      y = 0;
      while(pcList[y])
      {
         if(GetAt(x) == pcList[y]) 
         {
            SetAt(x, cChar);
            iNum++;
         }
         y++;
      }
   }
   return(iNum);
}
