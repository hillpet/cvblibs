/*  (c) Copyright:  2011 PATRN.NL
**
**  $Workfile:  NvmStorage.cpp $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Implementation of the NVM permanent storage class
**
**  Entries:
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       06 Jun 2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "CvbFiles.h"
#include "NvmStorage.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif



int      CNvmStorage::iInstance;
CString *CNvmStorage::clNVstore;
int      CNvmStorage::iNumWnvms;

IMPLEMENT_SERIAL(CNvmStorage, CObject, 0)

//
// This is the storage class for Non volatile data in any form. All data is stored
// into a CString array in ASCII format.
//
//  To store/retrieve :
//
//  Cstring data:       NvmPut(ID, clString)
//                      NvmGet(ID, &clString)
//  Bool:               NvmPut(ID, fFlag)
//                      NvmGet(ID, &fFlag)
//  u_int32:            NvmPut(ID, iNr)
//                      NvmGet(ID, &iNr)
//  ASCIIz              NvmPut(ID, pzChar)
//                      NvmGet(ID, pzChar) NOTE: pzChar MUST be preformatted to hold the data (with non-zero data).
//  Array of u_int8:    NvmPut(ID, pubData, size)
//                      NvmGet(ID, pubData, size)
//
CNvmStorage::CNvmStorage()
{
   if(iInstance != 0)
   {
      iInstance++;
   }
}

CNvmStorage::CNvmStorage(int iNum)
{
   if(iInstance == 0)
   {
      iInstance++;
      iNumWnvms  = iNum;
      clNVstore = new(CString[iNum]);
   }
}

CNvmStorage::~CNvmStorage()
{
   iInstance--;
   if(iInstance ==0)
   {
      delete(clNVstore);
   }
}

//
//  Function:   NvmRead
//  Purpose:   Read the data file into the NVM
//
//  Parms:     file
//  Returns:   TRUE if OKee opened
//
BOOL CNvmStorage::NvmRead(CString *pclFilename)
{
   BOOL         fCC = FALSE;
   CFile        clFile;

   if(clFile.Open(pclFilename->GetBuffer(), CFile::modeReadWrite, NULL) )
   {
      //
      // File is OKee: read it
      //
      CArchive clArchive(&clFile, CArchive::load);
      Serialize(clArchive);
      fCC = TRUE;
   }
   return(fCC);
}

//
//  Function:  NvmWrite
//  Purpose:   write the NVM to the data file
//
//  Parms:     filename
//  Returns:
//
BOOL CNvmStorage::NvmWrite(CString *pclFilename)
{
   CFile   clFile;
   //CString clStr;

   //
   // if File does not exist: make a new one
   //
   int iLog = clFile.Open(pclFilename->GetBuffer(), CFile::modeReadWrite|CFile::modeCreate, NULL);

   CArchive clArchive(&clFile, CArchive::store);
   Serialize(clArchive);

   //clStr.Format(_T("Saving config file as %s"), clFile.GetFilePath());
   //AfxMessageBox(clStr, MB_OK|MB_ICONEXCLAMATION);

   return(TRUE);
}

//
//  Function:  NvmPut
//  Purpose:   Put a value into a NVM storage location
//
//  Parms:     NVM-ID, CString with the data to store
//  Returns:
//
void CNvmStorage::NvmPut(int iNvmNr , CString s)
{
	if(iNvmNr < iNumWnvms)
	{
		clNVstore[iNvmNr] = s;
	}
}

//
//  Function:  NvmPut
//  Purpose:   Put a value into a NVM storage location
//
//  Parms:     NVM-ID, BOOL to store
//  Returns:
//
void CNvmStorage::NvmPut(int iNvmNr , BOOL fFlag)
{
	if(iNvmNr < iNumWnvms)
	{
		if(fFlag)
		{
			clNVstore[iNvmNr] = "1";
		}
		else
		{
			clNVstore[iNvmNr] = "0";
		}
	}
}

//
//  Function:  NvmPut
//  Purpose:   Put a value into a NVM storage location
//
//  Parms:     NVM-ID, Byte to store
//  Returns:
//
void CNvmStorage::NvmPut(int iNvmNr , u_int8 ubNr)
{

	if(iNvmNr < iNumWnvms)
	{
		CString s;

		s.Format(_T("%x"), ubNr);

		clNVstore[iNvmNr] = s;
	}
}

//
//  Function:  NvmPut
//  Purpose:   Put a value into a NVM storage location
//
//  Parms:     NVM-ID, Ptr to asciiZ with the textstring to store
//  Returns:
//
void CNvmStorage::NvmPut(int iNvmNr , char *pcData)
{
	if(iNvmNr < iNumWnvms)
	{
		clNVstore[iNvmNr] = pcData;
	}
}

//
//  Function:  NvmPut
//  Purpose:   Put a value into a NVM storage location
//
//  Parms:     NVM-ID, Ptr to Byte array plus number to store
//  Returns:
//
void CNvmStorage::NvmPut(int iNvmNr , u_int8 *pubNr, int iSize)
{

	if(iNvmNr < iNumWnvms)
	{
		CString s, stotal;
        int     iNr=0;

        while(iSize--)
        {
		    s.Format(_T("%02x,"), (u_int8) pubNr[iNr++]);
            stotal += s;
        }
        //
        // s = "xx,xx,xx,xx,xx,xx,xx,xx,xx, .... ,xx,"
        //
   		clNVstore[iNvmNr] = stotal;
	}
}

//
//  Function:  NvmGet
//  Purpose:   Get a value from a NVM storage location
//
//  Parms:     NVM-ID, Ptr to bool to receive the data
//  Returns:   TRUE if data correctly read
//
BOOL CNvmStorage::NvmGet(int iNvmNr, BOOL *pfFlag)
{
	if(pfFlag && (iNvmNr < iNumWnvms) )
	{
		int iNr, iResult;

    	*pfFlag = FALSE;

		iNr = sscanf(clNVstore[iNvmNr].GetBuffer(10), "%d", &iResult);
		if(iNr == 1)
		{
			*pfFlag = (BOOL) iResult;
			return(TRUE);
		}
	}
	return(FALSE);
}

//
//  Function:  NvmGet
//  Purpose:   Get a value from a NVM storage location
//
//  Parms:     NVM-ID, Ptr to Byte to receive the data
//  Returns:   TRUE if data correctly read
//
BOOL CNvmStorage::NvmGet(int iNvmNr, u_int8 *pubDest)
{
	if(pubDest && (iNvmNr < iNumWnvms) )
	{
		int iNr, iResult;

    	*pubDest = 0;

		iNr = sscanf(clNVstore[iNvmNr].GetBuffer(10), "%x", &iResult);
		if(iNr == 1)
		{
			*pubDest = (u_int8) iResult;
			return(TRUE);
		}
	}
	return(FALSE);
}

//
//  Function:  NvmGet
//  Purpose:   Get text from a NVM storage location
//
//  Parms:     NVM-ID, Ptr to textbuffer to receive the data
//             The buffer MUST be pre-formatted with 0x20 to take the data.
//  Returns:   TRUE if data correctly read
//
BOOL CNvmStorage::NvmGet(int iNvmNr, char *pcDest)
{
   BOOL     fCC = FALSE;
   char    *pcBuffer;
   size_t   iSize = 0;

   //
   // Get NVM data in the user buffer.
   //
	if(pcDest && (iNvmNr < iNumWnvms) )
	{
      // Count the destination size
      pcBuffer = pcDest;
      while(*pcBuffer++ == 0x20) iSize++;
      //copy the storage into the buffer
      strncpy(pcDest, clNVstore[iNvmNr], iSize);
      fCC = TRUE;
   }
   return(fCC);
}

//
//  Function:  NvmGet
//  Purpose:   Get byte array from a NVM storage location
//
//  Parms:     NVM-ID, Ptr to buffer to receive the data, number of members
//  Returns:   TRUE if data correctly read
//
BOOL CNvmStorage::NvmGet(int iNvmNr, u_int8 *pcDest, int iSize)
{
   BOOL fCC = TRUE;
   int     iNr;
   int     iResult;
   char   *pcBuffer;


	if(pcDest && (iNvmNr < iNumWnvms) )
	{
        pcBuffer = clNVstore[iNvmNr].GetBuffer();
        //
        // pcBuffer -> "xx,xx,xx,xx,xx,xx,xx,xx,xx, .... ,xx,"
        //
        while(fCC && iSize--)
        {
		    iNr = sscanf(pcBuffer, "%x", &iResult);
		    if(iNr == 1)
		    {
			    *pcDest++ = (char) (iResult & 0xff);
                pcBuffer +=3;
		    }
            else
            {
                fCC = FALSE;
            }
        }
	}
	return(fCC);
}

//
//  Function:  NvmGet
//  Purpose:   Get CString from a NVM storage location
//
//  Parms:     NVM-ID, Ptr to CString receive the data
//  Returns:   TRUE if data correctly read
//
BOOL CNvmStorage::NvmGet(int iNvmNr, CString *pclStr)
{
	if(pclStr && (iNvmNr < iNumWnvms) )
	{
    	*pclStr = "";

		*pclStr = clNVstore[iNvmNr];
		return(TRUE);
	}
	return(FALSE);
}

//
//  Function:  Serialize
//  Purpose:   Serialize an Archive from/into a CString array
//
//  Parms:     Archive
//  Returns:
//
void CNvmStorage::Serialize(CArchive &ar)
{
	int		     iNvmNr=0;

	CObject::Serialize(ar);

	if( ar.IsStoring() )
	{
		while( iNvmNr<iNumWnvms )
		{
		    ar << clNVstore[iNvmNr];
            iNvmNr++;
		}
	}
	else
	{
		while( iNvmNr<iNumWnvms )
		{
			ar >> clNVstore[iNvmNr];
            iNvmNr++;
		}
	}
}

//
//  Function:  NvmGet
//  Purpose:   Get number of elements in the NVM storage
//
//  Parms:
//  Returns:   Number 0...?
//
int CNvmStorage::NvmGetSize(int iNvmNr)
{
    int iSize = 0;

	if(iNvmNr < iNumWnvms)
	{
        iSize = clNVstore[iNvmNr].GetLength();
	}
	return(iSize);
}

//
//  Function:  NvmCheckArchive
//  Purpose:   Check if the archive file matches the format of the actual archive
//
//  Parms:     File name of the archive
//  Returns:   Number of elements or -1 if error
//
int CNvmStorage::NvmCheckArchive(CString *pclArchive)
{
   char  cBuffer[2];
   int   iNr = 0;
   int   iRead;
   unsigned long ulSkip;
   long  lAct, lSeek = 0;
   CFile clFile;

   if(clFile.Open(pclArchive->GetBuffer(), CFile::modeRead, NULL) )
   {
      //
      // File is OKee: read it
      //
      do
      {
         lAct = (long) clFile.Seek(lSeek, CFile::begin);
         if(lSeek == lAct)
         {
            iRead = clFile.Read(cBuffer, 1);
            if(iRead == 1)
            {
               ulSkip  = CVB_ulong_get((u_int8 *)cBuffer, 0x000000FF, 24);
               if(ulSkip == 0x000000FF)
               {
                  iRead = clFile.Read(cBuffer, 1);
                  if(iRead == 1) ulSkip  = CVB_ulong_get((u_int8 *)cBuffer, 0x000000FF, 24);
                  iRead = clFile.Read(cBuffer, 1);
                  if(iRead == 1) ulSkip += CVB_ulong_get((u_int8 *)cBuffer, 0x0000FF00, 16);
                  ulSkip += 3;
               }
               else
               {
                  ulSkip += 1;
               }
               lSeek  += (long) ulSkip;
               iNr++;
            }
            else
            {
               // All entries read: Exit
               break;
            }
         }
         else
         {
            // Problems with the archive
            iNr = -1;
            break;
         }
      }
      while(iRead);
      clFile.Close();
   }
   return(iNr);
}
