/*  (c) Copyright:  2009  CVB, Confidential Data
**
**  $Workfile:          Cmdline.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Commandline parameters
**
**
**
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       16 Aug 2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_COMMANDLINE_H_)
#define _COMMANDLINE_H_


typedef struct CMDLINE
{
   BOOL     fPresent;
   BOOL     fOn;
   CString  *pclValue;
}  CMDLINE;

//
// Command line options
//
class CMyCommandLineInfo : public CCommandLineInfo
{
public:
   int            iInstance;                 // Count instances
   BOOL           fSuccess;                  // all switches ok
   static CMDLINE stCmdLine[52];             // all a..z and A..Z switches
   static CString clStrCmdLineAll;           // Constructed command line
   //
private:
   virtual void ParseParam       (LPCTSTR, BOOL, BOOL);
   int          MakeIndex        (char);
   int          GetParmLength    (const char *);
   //

public:
   CMyCommandLineInfo(void);
   //
   void     Cleanup              (void);
   void     GetCommandline       (CString *);
   BOOL     GetSwitchValue       (char, CString *);
   BOOL     IsSwitchPresent      (char);
   BOOL     IsSwitchOn           (char);
   //
};

#endif   //_COMMANDLINE_H_
